package Model;

public class Card {
	private int balance;
	private int payin;
	private int payout;
	
	public Card(int balance){
		this.balance = balance;		
	}
	
	public void deposit(int payin){
		this.payin = payin;
		balance += payin;
	}
	
	public void withdraw(int payout){
		this.payout = payout;
		balance -= payout;
	}
	
	public String toString1(){
		
	return "Balance = " + balance;				
	}
	
	public String toString2(){
		return "Deposit = " + payin;
	}
	
	public String toString3(){
		return "Withdraw = " + payout;
	}
}

