package Control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import Model.Card;
import View.GUI;

public class Controller {
	GUI frame;
	ActionListener list;
	
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller();
	}
	
	public Controller(){
		frame = new GUI();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400,400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();		
	}
	
	public void setTestCase(){		
		Card stu = new Card(0);
		frame.setResult(stu.toString1());
		stu.deposit(200);
		frame.extendResult(stu.toString2());
		frame.extendResult(stu.toString1());
		stu.withdraw(50);
		frame.extendResult(stu.toString3());
		frame.extendResult(stu.toString1());
	}
	
}

