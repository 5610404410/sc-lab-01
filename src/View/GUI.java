package View;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI extends JFrame{	
	private JLabel showName;
	private JTextArea showResult;
	private JButton endButton;
	private String str;
	
	public GUI(){
		createFrame();
	}
	public void createFrame(){
		showName = new JLabel("Machine");
		showResult = new JTextArea("here");
		endButton = new JButton("end program");
		setLayout(new BorderLayout());
		add(showName, BorderLayout.NORTH);
		add(showResult, BorderLayout.CENTER);
		add(endButton, BorderLayout.SOUTH);
	}
	
	public void setName(String s){
		showName.setText(s);
	}
	
	public void setResult(String str){
		this.str = str;
		showResult.setText(this.str);
	}
	
	public void extendResult(String str){
		this.str = this.str +"\n"+ str;
		showResult.setText(this.str);
	}
	
	public void setListener(ActionListener list){
		endButton.addActionListener(list);
	}
}

